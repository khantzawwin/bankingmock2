//
//  FireStoreHelper.m
//  banking
//
//  Created by Khant Zaw Win on 16/5/21.
//

#import "FireStoreHelper.h"

@implementation FireStoreHelper

+ (FireStoreHelper *)sharedInstance
{
    static FireStoreHelper* shared = nil;
    static dispatch_once_t pred;
    dispatch_once(&pred, ^{
        shared = [[self alloc] init];
        shared.defaultDB = [FIRFirestore firestore];
    });
    return shared;
}

- (void)addOrUpdateUserWithEmail: (NSString *)email user:(NSDictionary *) user andDelegate: (id<FireStoreHelperDelegate>)delegate {
    
    [[[self.defaultDB collectionWithPath:@"users_2"] documentWithPath:email] setData:user merge:YES
     completion:^(NSError * _Nullable error) {
      if (error != nil) {
        if ([delegate respondsToSelector:@selector(errorWithMessage:)])
            [delegate errorWithMessage:error];
      } else {
        if ([delegate respondsToSelector:@selector(addOrUpdateUserSuccess)])
            [delegate addOrUpdateUserSuccess];
      }
    }];
}

- (void)getUserByEmail: (NSString *)email withDelegate:(id<FireStoreHelperDelegate>)delegate {
    FIRDocumentReference *docRef = [[self.defaultDB collectionWithPath:@"users_2"] documentWithPath:email];

    [docRef getDocumentWithCompletion:^(FIRDocumentSnapshot *snapshot, NSError *error) {
      if (snapshot != NULL) {
          if ([delegate respondsToSelector:@selector(getUserInfo:)])
              [delegate getUserInfo: snapshot.data];
      } else {
         if ([delegate respondsToSelector:@selector(errorWithMessage:)])
             [delegate errorWithMessage:error];
      }
    }];
}

- (void) makeTransactionWithSenderEmail: (NSString *)senderEmail AndUpdateSenderBalance: (NSNumber *) senderBalance AndRecipientEmail: (NSString *)recipientEmail AndRecipientBalance: (NSNumber *)recipientBalance AndDelegate:(id<FireStoreHelperDelegate>)delegate{
    FIRWriteBatch *batch = [self.defaultDB batch];
    
    FIRDocumentReference *sender =
        [[self.defaultDB collectionWithPath:@"users_2"] documentWithPath:senderEmail];
    [batch updateData:@{ @"balance": senderBalance } forDocument:sender];
    
    FIRDocumentReference *recipient =
        [[self.defaultDB collectionWithPath:@"users_2"] documentWithPath:recipientEmail];
    [batch updateData:@{ @"balance": recipientBalance } forDocument:recipient];
    
    [batch commitWithCompletion:^(NSError * _Nullable error) {
      if (error != nil) {
          if ([delegate respondsToSelector:@selector(errorWithMessage:)])
              [delegate errorWithMessage:error];
      } else {
          if ([delegate respondsToSelector:@selector(transactionSuccess)])
              [delegate transactionSuccess];
      }
    }];
}



@end
