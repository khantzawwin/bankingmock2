//
//  Helper.m
//  banking
//
//  Created by Khant Zaw Win on 16/5/21.
//

#import "Helper.h"

@implementation Helper

+(void)ShowErrorMsgDialogAlert:(NSString *)msg andView:(UIViewController *)controller
{
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"Banking" message:msg preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction * okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                        style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction *action){}];
    
    [alertVC addAction:okAction];
    [controller presentViewController:alertVC animated:YES completion:nil];
}
@end
