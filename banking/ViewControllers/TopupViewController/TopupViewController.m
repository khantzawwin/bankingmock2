//
//  TopupViewController.m
//  banking
//
//  Created by Khant Zaw Win on 16/5/21.
//

#import "TopupViewController.h"
#import "FireStoreHelper.h"
#import "Helper.h"

@interface TopupViewController () <UITextFieldDelegate,FireStoreHelperDelegate>
@property(weak, nonatomic) IBOutlet UITextField *amountTextField;

@end

@implementation TopupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.amountTextField.delegate = self;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [UIView animateWithDuration:.3 animations:^{
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -200., self.view.frame.size.width, self.view.frame.size.height);
    }];
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [UIView animateWithDuration:.3 animations:^{
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +200., self.view.frame.size.width, self.view.frame.size.height);
    }];
}

-(void)dismissKeyboard
{
    [self.amountTextField resignFirstResponder];
}

- (IBAction)onClickTopup:(id)sender{
    double current_balance = [self.dbUser[@"balance"] doubleValue];
    double topup_value = [self.amountTextField.text doubleValue];
    NSNumber * total_balance = [[NSNumber alloc]initWithDouble:(current_balance+topup_value)];
    [self.dbUser setValue:total_balance forKey:@"balance"];
    
    [[FireStoreHelper sharedInstance] addOrUpdateUserWithEmail:self.dbUser[@"email"] user:self.dbUser andDelegate:self];
}

- (void)addOrUpdateUserSuccess{
    [self.delegate reloadData];
    [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
}

- (void)errorWithMessage:(NSError *)message{
    [Helper ShowErrorMsgDialogAlert:message.localizedDescription andView:self];
}

@end
