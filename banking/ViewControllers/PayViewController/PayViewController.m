//
//  PayViewController.m
//  banking
//
//  Created by Khant Zaw Win on 16/5/21.
//

#import "PayViewController.h"
#import "FireStoreHelper.h"
#import "Helper.h"

@interface PayViewController () <UITextFieldDelegate,FireStoreHelperDelegate>

@property(weak, nonatomic) IBOutlet UITextField *recipientTextField;
@property(weak, nonatomic) IBOutlet UITextField *amountTextField;

@end

@implementation PayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.recipientTextField.delegate = self;
    self.amountTextField.delegate = self;
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [UIView animateWithDuration:.3 animations:^{
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -200., self.view.frame.size.width, self.view.frame.size.height);
    }];
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [UIView animateWithDuration:.3 animations:^{
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +200., self.view.frame.size.width, self.view.frame.size.height);
    }];
}

-(void)dismissKeyboard
{
    [self.recipientTextField resignFirstResponder];
    [self.amountTextField resignFirstResponder];
}

- (IBAction)onClickPay:(id)sender{
    if(self.recipientTextField.text.length && self.amountTextField.text.length)
        [[FireStoreHelper sharedInstance]getUserByEmail:self.recipientTextField.text withDelegate:self];
    else
        [Helper ShowErrorMsgDialogAlert:@"Please enter recipient email and amount." andView:self];
}

- (void)getUserInfo: (NSDictionary *)userInfo{
    if(userInfo)
    {
        double sender_current_balance = [self.dbUser[@"balance"] doubleValue];
        double amount = [self.amountTextField.text doubleValue];
        NSNumber * sender_update_balance = [[NSNumber alloc]initWithDouble:(sender_current_balance - amount)];
        
        double recipient_current_balance = [userInfo[@"balance"] doubleValue];
        NSNumber * recipient_update_balance = [[NSNumber alloc]initWithDouble:(recipient_current_balance + amount)];
        
        [[FireStoreHelper sharedInstance]makeTransactionWithSenderEmail:self.dbUser[@"email"] AndUpdateSenderBalance:sender_update_balance AndRecipientEmail:userInfo[@"email"] AndRecipientBalance:recipient_update_balance AndDelegate:self];
    }
    else
    {
        [Helper ShowErrorMsgDialogAlert:@"Recipient email not found." andView:self];
    }
}


- (void)transactionSuccess{
    [self.delegate reloadData];
    [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
}

- (void)errorWithMessage:(NSError *)message{
    [Helper ShowErrorMsgDialogAlert:message.localizedDescription andView:self];
}

@end
