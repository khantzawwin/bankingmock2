//
//  HomeViewController.m
//  banking
//
//  Created by Khant Zaw Win on 16/5/21.
//

#import "HomeViewController.h"
#import <FirebaseAuth.h>
#import "FireStoreHelper.h"
#import "TopupViewController.h"
#import "PayViewController.h"
#import "Helper.h"

@interface HomeViewController () <FireStoreHelperDelegate,TopupViewControllerDelegate, PayViewControllerDelegate>

@property(weak, nonatomic) IBOutlet UILabel *nameLabel;
@property(weak, nonatomic) IBOutlet UILabel *balanceLabel;
@property(weak, nonatomic) IBOutlet UILabel *debtLabel;
@property(strong, nonatomic) NSDictionary *dbUser;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    NSString * email = [FIRAuth auth].currentUser.email;
    [[FireStoreHelper sharedInstance] getUserByEmail:email withDelegate:self];
    
}

- (void)getUserInfo: (NSDictionary *)userInfo{
    if(userInfo)
    {
        self.dbUser = userInfo;
        NSDictionary * dbUser = userInfo;
        
        self.nameLabel.text = [NSString stringWithFormat:@"Hello, %@",[dbUser objectForKey:@"name"]];
        
        double balance = 0;
        balance = [[dbUser objectForKey:@"balance"] doubleValue];
        
        if (balance < 0)
        {
            self.balanceLabel.text = [NSString stringWithFormat:@"Balance : 0.00 SGD"];
            self.debtLabel.text = [NSString stringWithFormat:@"Debt : %.2f SGD",(balance * -1)];
        }
        else
        {
            self.balanceLabel.text = [NSString stringWithFormat:@"Balance : %.2f SGD",balance];
            self.debtLabel.text = [NSString stringWithFormat:@"Debt : 0.00 SGD"];
        }
    }
}

- (IBAction)onClickLogout:(id)sender{
    [[FIRAuth auth] signOut:nil];
}

- (IBAction)onClickTopup:(id)sender{
    TopupViewController * vc = [[TopupViewController alloc]init];
    vc.delegate = self;
    vc.dbUser = self.dbUser;
    [self presentViewController:vc animated:YES completion:^{
        
    }];
}

- (IBAction)onClickPay:(id)sender{
    PayViewController * vc = [[PayViewController alloc]init];
    vc.delegate = self;
    vc.dbUser = self.dbUser;
    [self presentViewController:vc animated:YES completion:^{
        
    }];
}

-(void)reloadData{
    NSString * email = [FIRAuth auth].currentUser.email;
    [[FireStoreHelper sharedInstance] getUserByEmail:email withDelegate:self];
}

- (void)errorWithMessage:(NSError *)message{
    [Helper ShowErrorMsgDialogAlert:message.localizedDescription andView:self];
}

@end
