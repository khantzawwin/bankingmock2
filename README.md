# Banking mock mobile application for iOS #

This project is the mock up project for basic money transfer across registered users.

### How to setup the project ###

* clone the project
* run ```pod install``` on project root folder
* run the project in XCode


### Recommendation ###

 **Firebase**: This project is using Firebase Authentication and Firebase Firestore. Therefore, if you change the bundle id, there can have some issue to run the project. To avoid the issue, please run with simulator without changing bundle id.
